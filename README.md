# IMS Base Classes

Base classes required for IMS project.


## License


  [AGPL-3.0-or-later](LICENSE)



## Contact

Alpha Fork Technologies

[connect@alphafork.com](mailto:connect@alphafork.com)
