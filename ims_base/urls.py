import importlib
import re

from django.apps import apps
from django.conf import settings as project_settings
from django.urls import include, path
from django.utils.module_loading import import_string
from rest_framework import routers

from .apis import BaseAPIViewSet

app_name = "base"
urlpatterns = []


def dynamic_urls_generator():
    app_lists = getattr(project_settings, "DYNAMIC_URLS_APPS_LISTS", ["DJ_IMS_APPS"])
    app_list = []
    for app_list_str in app_lists:
        app_list += getattr(project_settings, app_list_str, [])
    excluded_apps = getattr(project_settings, "DYNAMIC_URLS_EXCLUDED_APPS", [])
    app_list = [app for app in app_list if app not in excluded_apps]
    app_list.remove("ims_base") if "ims_base" in app_list else None
    app_urls = []
    for app_str in app_list:
        app = apps.get_app_config(app_str)
        app_label = app.label
        app_name = app_label.removeprefix("ims_").replace("_", "-")
        try:
            app_urlconf = importlib.import_module(app_label + ".urls")
        except ImportError:
            pass
        else:
            app_urls.append(path(app_name + "/", include(app_urlconf)))
        router = routers.SimpleRouter()
        app_models = app.get_models()
        for model in app_models:
            object_name = model._meta.object_name
            viewset_name = object_name + "API"
            try:
                viewset = import_string(app_label + ".apis." + viewset_name)
            except ImportError:
                pass
            else:
                model_slug = re.sub(r"(?<!^)(?=[A-Z])", "-", object_name).lower()
                router.register(r"" + model_slug + "", viewset, model_slug)
            if router.urls:
                app_urls.append(path(app_name + "/", include(router.urls)))
    return app_urls


autofetch_viewsets = getattr(project_settings, "AUTOFETCH_VIEWSETS", True)
if autofetch_viewsets:
    urlpatterns = dynamic_urls_generator() or []

router = routers.SimpleRouter()
router.register(r"", BaseAPIViewSet, "page")

urlpatterns += [
    path("<slug:page_slug>/<slug:subpage_slug>/", include(router.urls)),
]
