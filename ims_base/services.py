import segno


def generate_qr_code_svg(string):
    return segno.make_qr(string).svg_inline(scale=10)
