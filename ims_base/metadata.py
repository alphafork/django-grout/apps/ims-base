from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions
from rest_framework.metadata import SimpleMetadata
from rest_framework.request import clone_request
from rest_framework.serializers import copy
from collections import OrderedDict

class BaseMetadata(SimpleMetadata):
    """
    Add relation type and related field to field information returned by
    OPTIONS response.
    """

    def determine_actions(self, request, view):
        """
        Method overridden to provide API schema in 'OPTIONS' response for users
        with access to corresponding 'GET' method in addition to 'PUT' and
        'POST' methods which are already handled by DRF.
        """
        actions = {}
        for method in {"GET", "PUT", "POST"} & set(view.allowed_methods):
            view.request = clone_request(request, method)
            try:
                # Test global permissions
                if hasattr(view, "check_permissions"):
                    view.check_permissions(view.request)
                # Test object permissions
                if method == "PUT" and hasattr(view, "get_object"):
                    view.get_object()
            except (exceptions.APIException, PermissionDenied, Http404):
                pass
            else:
                # If user has appropriate permissions for the view, include
                # appropriate metadata about the fields that should be supplied.
                serializer = view.get_serializer()
                actions[method] = self.get_serializer_info(serializer, method)
            finally:
                view.request = request

        return actions

    def get_model_url(self, queryset):
        """
        Converts 'ims_<app>.<Model>.objects' to '/<app>/<model>'
        """
        model_url = queryset.lower()
        model_url = model_url.replace("ims_", "")
        model_url = model_url.replace(".objects", "")
        model_url = model_url.replace(".", "/")
        model_url = "/" + model_url
        return model_url

    def get_field_info(self, field):
        field_info = super().get_field_info(field)
        native_type = type(field).__name__

        if field_info["type"] == "field":
            field_info["native_type"] = native_type

        if native_type == "PrimaryKeyRelatedField":
            queryset = field.queryset
            related_model_url = self.get_model_url(str(queryset))
            field_info["related_model_url"] = related_model_url

        if native_type == "ManyRelatedField":
            queryset = field.child_relation.queryset
            related_model_url = self.get_model_url(str(queryset))
            field_info["related_model_url"] = related_model_url

        return field_info

    def get_serializer_info(self, serializer, method):
        # retrieve meta kwargs if provided

        if hasattr(serializer, "get_extra_meta"):
            extra_meta = copy.deepcopy(serializer.get_extra_meta())
        else:
            extra_meta = {}

        read_only_extra_meta = {}
        if method == "GET" and hasattr(serializer, "get_read_only_extra_meta"):
            read_only_extra_meta = OrderedDict(
                copy.deepcopy(serializer.get_read_only_extra_meta())
            )

        # call the original implementation
        field_infos = super().get_serializer_info(serializer)

        # and add meta kwargs to the fields
        for field_name, field in field_infos.items():
            if field_name in extra_meta:
                for key, value in extra_meta[field_name].items():
                    field[key] = value
        field_infos = OrderedDict(
            {**field_infos, **read_only_extra_meta}
        )
        return field_infos
