from django.conf import settings as project_settings
from django.contrib.contenttypes.models import ContentType
from rest_framework.exceptions import NotFound
from rest_framework.views import APIView


class GetModelFromSlugMixin(APIView):
    model_class = None

    def initial(self, request, *args, **kwargs):
        self.get_slugs()
        self.get_model_class()
        super().initial(request, *args, **kwargs)

    def get_slugs(self):
        self.page_slug = self.request.path.split("/")[1]
        self.subpage_slug = self.request.path.split("/")[2]

    def get_model_class(self):
        if not self.model_class:
            project_prefix = getattr(project_settings, "PROJECT_PREFIX", "ims")
            app_name = self.page_slug.replace("-", "_")
            app_label = f"{project_prefix}_{app_name}"
            model_name = self.subpage_slug.replace("-", "")
            content_type = None
            try:
                content_type = ContentType.objects.get(
                    app_label=app_label, model=model_name
                )
            except ContentType.DoesNotExist:
                raise NotFound(
                    detail="API does not exist.",
                    code=404,
                )
            else:
                model_class = content_type.model_class()
                self.model_class = model_class
                return model_class
