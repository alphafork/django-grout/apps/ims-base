from collections import OrderedDict

from django.utils.datastructures import copy
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework.serializers import ModelSerializer, model_meta
from reusable_models import get_model_from_string


class BaseModelSerializer(ModelSerializer):
    class Meta:
        fields = "__all__"
        excluded_fields = ["created_at", "created_by", "updated_at", "updated_by"]
        extra_meta = {}

    def get_extra_meta(self):
        return self.Meta.extra_meta

    def get_field_names(self, declared_fields, info):
        exclude = getattr(self.Meta, "exclude", None)
        assert exclude is None, (
            "`exclude` field can not be used with '%s', "
            "please use `excluded_fields` instead." % self.__class__.__name__
        )
        fields = getattr(self.Meta, "fields", None)
        excluded_fields = getattr(self.Meta, "excluded_fields", [])

        if not fields or fields == "__all__":
            fields = super().get_field_names(declared_fields, info)
        if isinstance(fields, str):
            fields = [fields]

        for excluded_field in excluded_fields:
            try:
                fields.remove(excluded_field)
            except ValueError:
                pass
        return fields

    def create(self, validated_data):
        if "created_by" in [f.name for f in self.Meta.model._meta.fields]:
            if hasattr(self.context, "request"):
                validated_data["created_by"] = self.context["request"].user
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if "updated_by" in [f.name for f in self.Meta.model._meta.fields]:
            if hasattr(self.context, "request"):
                validated_data["updated_by"] = self.context["request"].user
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        for field in self.Meta.model._meta.fields:
            if field.many_to_one:
                field_value = getattr(instance, field.name, "")
                data[field.name + "_name"] = field_value.__str__()
        return data


class BaseWritableNestedModelSerializer(
    BaseModelSerializer, WritableNestedModelSerializer
):
    _nested_fields_tree = OrderedDict()

    def __init__(self, instance=None, **kwargs):
        self._nested_fields_tree = OrderedDict()
        super().__init__(instance, **kwargs)

    class Meta(BaseModelSerializer.Meta):
        nest_depth = 0

    def get_field_names(self, declared_fields, info):
        fields = super().get_field_names(declared_fields, info)

        for field in fields:
            parent_field = field.split("__")[0]
            self._nested_fields_tree[parent_field] = [
                field.removeprefix(parent_field + "__")
                if parent_field is not field
                else None
            ]
        fields = list(self._nested_fields_tree)
        return fields

    def get_fields(self):
        fields = super().get_fields()
        declared_fields = copy.deepcopy(self._declared_fields)
        model = getattr(self.Meta, "model")
        info = model_meta.get_field_info(model)
        field_names = self.get_field_names(declared_fields, info)
        field_data = self._get_model_fields(field_names, declared_fields, {})

        for field_name in field_names:
            if (
                field_data[field_name].is_relation
                and not field_data[field_name].many_to_many
            ):
                nest_depth = getattr(self.Meta, "nest_depth", None)
                if nest_depth:
                    counter = nest_depth - 1
                    field_instance = self.create_child_serializer(
                        field_name, counter, "__all__"
                    )
                    fields[field_name] = field_instance
                else:
                    nested_fields_list = self._nested_fields_tree[field_name]
                    if nested_fields_list != [None]:
                        field_instance = self.create_child_serializer(
                            field_name, None, nested_fields_list
                        )
                        fields[field_name] = field_instance

        return fields

    def create_child_serializer(self, field_name, counter, nested_fields_list):
        class GenericChildSerializer(BaseWritableNestedModelSerializer):
            _nested_fields_tree = OrderedDict()

            class Meta(BaseWritableNestedModelSerializer.Meta):
                exclude = None
                fields = nested_fields_list
                nest_depth = counter
                model = get_model_from_string(field_name.upper())

        return GenericChildSerializer()
