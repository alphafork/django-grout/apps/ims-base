import json

from django.utils.module_loading import import_string
from ims_api_permission.apis import DjangoModelPermissionsStrict
from rest_framework.filters import BaseFilterBackend
from rest_framework.generics import ListAPIView, RetrieveAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from reusable_models import get_model_from_string

from .mixins import GetModelFromSlugMixin
from .serializers import BaseModelSerializer, BaseWritableNestedModelSerializer

APIType = get_model_from_string("API_TYPE")
APIMethod = get_model_from_string("API_METHOD")
APIGroupPermission = get_model_from_string("API_GROUP_PERMISSION")


class GenericFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "filter_by" in request.query_params:
            filter_map = json.loads(request.query_params["filter_by"])
            return queryset.filter(**filter_map)
        return queryset


class BaseAPIView(APIView):
    is_permissions_managed = True
    permission_classes = [IsAuthenticated]


class BaseListView(ListAPIView, APIView):
    permission_classes = [IsAuthenticated, DjangoModelPermissionsStrict]

    def get_serializer_class(self):
        if not self.serializer_class:
            self.serializer_class = BaseModelSerializer
            self.serializer_class.Meta.model = self.model_class
        return super().get_serializer_class()


class BaseRetrieveView(RetrieveAPIView, BaseAPIView):
    permission_classes = [IsAuthenticated, DjangoModelPermissionsStrict]

    def get_serializer_class(self):
        if not self.serializer_class:
            self.serializer_class = BaseModelSerializer
            self.serializer_class.Meta.model = self.model_class
        return super().get_serializer_class()


class BaseAPIViewSet(BaseAPIView, GetModelFromSlugMixin, ModelViewSet):
    filter_backends = [GenericFilter]
    permission_classes = [IsAuthenticated, DjangoModelPermissionsStrict]

    def get_queryset(self):
        queryset = self.queryset
        if not queryset:
            model = self.model_class
            queryset = model.objects.all()
            self.queryset = queryset
        return super().get_queryset()

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if not serializer_class:
            serializer_name = self.model_class._meta.object_name + "Serializer"
            serializer_path = self.model_class._meta.app_label + ".serializers"
            try:
                serializer_class = import_string(
                    serializer_path + "." + serializer_name
                )
            except ImportError:
                serializer_class = BaseModelSerializer
            self.serializer_class = serializer_class
        self.serializer_class.Meta.model = self.model_class
        return super().get_serializer_class()


class BaseNestedWritableAPI(BaseAPIViewSet):
    serializer_class = BaseWritableNestedModelSerializer


class RelatedFieldFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if not request.user.is_superuser:
            queryset = queryset.filter(**view.get_filter_kwargs())
        return queryset


class BaseRelatedFieldViewSet(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [RelatedFieldFilter]

    def get_filter_kwargs(self):
        filter_kwargs = {self.lookup_field: self.kwargs.get(self.lookup_field)}
        return filter_kwargs

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset, **self.get_filter_kwargs())
        self.check_object_permissions(self.request, obj)
        return obj


class RelatedFieldExcludedRelatedFieldViewSet(BaseRelatedFieldViewSet):
    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        self.serializer_class.Meta.excluded_fields += [self.lookup_field]
        return serializer_class

    def perform_create(self, serializer):
        serializer.save(**self.get_filter_kwargs())
        return super().perform_create(serializer)

    def create(self, request, *args, **kwargs):
        if self.model_class.objects.filter(**self.get_filter_kwargs()).exists():
            return self.update(request, *args, **kwargs)
        return super().create(request, *args, **kwargs)
