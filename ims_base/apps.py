from django.apps import AppConfig


class ImsBaseConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_base"

    model_strings = {
        "MODELSLUG": "ModelSlug",
    }
